# How to add Prosthetic Devices #

BioPatRec offers currently a standard configuration (movements/motors/sensors definitions and control/acquisition messages) that can be used as a reference communication protocol for controlling prosthetic devices. The idea is that if you are developing your robotic device and you are thus free to decide the communication protocol to use you can follow our suggested protocol and this will give you full compatibility with BioPatRec. In this case, you will only need to create the movements.def, motors.def, and sensors.def files as described in the following pages:

 * movements.def and motors.def: [BioPatRec and Movements](Movements_Protocol.md) page
 * sensors.def: [BioPatRec and Sensors](sensors protocol.md) page

and put all files in the same folder (referred here as "MyDevice") somewhere in your PC. Please note that you have to use exactly the suggested names for the definition files. Including (or not including) the new definition files in your Matlab working path has no influence. On the contrary, the "VRE" folder (located into "Control" folder) must always be included in the path because motors_VRE and movements_VRE represent the default definition files.

You are now ready to control your device through the GUI_TestPatRec_Mov2Mov GUI. In this GUI, the "Prosthetic device" area contains the controls needed to communicate with your device. The first step is to select the right folder (we called it MyDevice) where your .def files are stored by pressing the "Select Folder" button. Then you can connect BioPatRec with your device. Serial and Wi-Fi connections are currently supported in BioPatRec. The Test button (calling the TestConnection routine) can be used to test if the bidirectional communication actually works. You also want to set the motors coupling flag to actually activate the movements.


## Additional steps for commercially available devices ##
In case you have no access to the device firmware and thus are not able to stick with the proposed protocol, then a few more steps are needed in order to be able to communicate with your device. In particular, you will have to create the functions needed to translate the information provided by BioPatRec to commands that are meaningful for your device. Those functions are

 * SendMotorCommand.m
 * ReadSensors.m
 * TestConnection.m

Similarly to the definition files, all of the functions will have to be saved in the MyDevice folder so that BioPatRec can retrieve them when you select this folder from GUI_TestPatRec_Mov2Mov. Please note that you can take the standard functions as a starting point to build your customized version.

### SendMotorCommand.m ###
When one movement request is issued, the SendMotorCommand function is called by the MotorsOn function as shown below:

    if ~SendMotorCommand(com, ctrl_type, motor_index, mov_dir, ctrl_val);
        disp('Failed'); % CHECK
        fclose(com);
    end

where
 * `com` is the communication object created when the connection is created
 * `ctrl_type` is the type of control to be used for that movement. It is either 0 (PWM control) or 1 (position control)
 * `motor_index` is the id of the motor to be moved, as defined in the motors.def file
 * `mov_dir` is the movement direction required for that movement and motor, as defined in the movements.def file. It can be either 0 or 1.
 * `ctrl_val` is the control variable. Depending on the control type this can be either a position or a PWM, but it will always range from 0 to 100 (it is normalized).

The SendMotorCommand function is also supposed to return 1 if the communication was successful, or 0 if it was not.

NOTES:
 * the SendMotorCommand function is also called by the MotorsOff function when the movement needs to be stopped. In this case, `ctrl_val` is set to 0.

### ReadSensors.m ###
When the Start button in the GUI_Sensors is pressed, a timer is started and the ReadSensors.m is called at each timer tick. The syntax is the following

    data(t,:)   = ReadSensors(obj, handles.sensors, handles.nrSensors);

where

 * `obj` is the communication object created when the connection is created (the same as `com` in SendMotorCommand)
 * `handles.sensors` is a structure containing the IDs of all the sensors to be read. The id of the sensor `n` is stored in the `handles.sensors(n).id` variable and is by default a **char** type. Thus you might want to transform it to double before using it in the communication.
 * `handles.nrSensors` containing the total number of sensors to be read.

The function returns a row array (sized 1xhandles.nrSensors) containing the readout from all sensors. This array is then stored in the `data`, used to plot the sensors readings.

### TestConnection.m ###
We kept the easy part for the last, so that you can relax! The TestConnection function is called when the Test button is pressed in the GUI_TestPatRec_Mov2Mov, as shown below:

    if TestConnection(handles.Control_obj)
        set(handles.t_msg,'String','Connection OK');
    else
        set(handles.t_msg,'String','Wrong connection');
        fclose(handles.Control_obj);
    end

as you can see, the function takes only one input argument (`handles.Control_obj`), which is the communication object created when the connection is created (remember the `com` argument in SendMotorCommand? same thing) and gives one output argument, which is `1` if the communication was successful and `0` if it was not. You are then completely free to test the communication as is more convenient for you. Usually, this is done by sending a command that requires the hardware to answer, and then check if the answer is received.

Now, as said before, you just need to put all of the three functions in the MyDevice folder, so that when you will select such folder from GUI_TestPatRec_Mov2Mov, those will be used in place of the standard ones.