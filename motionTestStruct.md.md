[recSession](recSession.md) -> [sigTreated](sigTreated.md) -> [sigFeatures](sigFeatures.md) -> [patRec](patRec.md) -> [motionTest](motionTest.md) || [tacTest](tacTest.md)

# motionTest #

  * sF (sampling frequency)
  * tW (time window)
  * trial(number of examination)
  * nR(number of repetition)
  * timeOut (time in which trial must be completed)
  * test
  * compRate (completion rate,the percentage of successfully completed    posture)
  * selTime(selection time, selected = expected movements)
  * selTimeTW(nTW when the selection time happen)
  * compTime (completion time, selected = expected movements)
  * compTimeTW(nTW when the complection time happen)
  * acc (accuracy per movement plus the over all accuracy in the last row)

see [Motion\_Test](Motion_Test.md) for more information.