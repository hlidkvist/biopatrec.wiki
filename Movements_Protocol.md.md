# BioPatRec and Movements #

BioPatRec has currently the following output options:

  * [VRE](VRE.md)
  * [Robotic devices](how to add prosthetic devices.md)

In order to know which movements are being used, an object is created and initialized during the connection. This object is then used to retrieve the values for what is to be sent to different outputs (VRE or device motors) in order to perform that specific movement.

New movements can always be defined by simply modifying the movement.def file. Please keep reading this page for further details on the movements definition file.


NOTES:
  * **If you want to use BioPatRec with different prosthetic hardware, follow this tutorial on [how to add prosthetic devices](how to add prosthetic devices.md).**
  * We are now using an object-oriented way to process the output movements. However, it is not absolutely necessary to use these objects and these can be bypassed. See notes at the bottom of this page.

# Protocol #

The protocol in use concurs with that of the [VRE\_Protocol](VRE_Protocol.md) and [Motors\_Protocol](Motors_Protocol.md). Please see each page for their specific protocol information.

## Movement Object ##

As stated above, each movement has its own object with the needed values for VRE and each motor involved. 
The movements are firstly initialized in the Load_patRec function thanks to the InitMovements call. 

They are retrieved from the movement.def file where each line represents a movement, the syntax is shown in the following table

\#1 | \#2 | \#3 | \#4 | \#5 
--- | --- | --- | --- | ---  
id | name | vre byte | vre direction | motorObjectIndex  motorObjectDirection

Each movement is then created using the movement class (movement.m).

The last parameters are an identifier to state which motors are connected to the movement and the directions towards which every motor should be moved in order to execute that specific movement. They are then activated when the requested movement is performed. It can be represented as a space-delimited list for multiple motors. In particular, the motorObjectIndex is the line number of the motors.def file in which said motor is defined, while the motorObjectDirection is, as said before, the direction of the movement.

Please check the example file below.

## Motors Object ##

The motors are firstly initialized in the _Load\_patRec_ function, before the movements. They are retrieved from _motors.def_ where each line represents a motor, the type of the motor and its position/speed during activation (in percentage).

\#1 | \#2 | \#3
--- | --- | --- 
|id|type|percentage of position or speed|

Each movement is then created using the motor class (motor.m).

**NOTE: If the order of the motors is changed, the _movements.def_ has to be modified accordingly as otherwise this will point to the wrong motor.**

# Using the Objects in GUI #
In the GUI for testing Pattern Recognition Mov2Mov, when a button is pressed, the value of the drop-down list is used to identify which movement in the list of movements has to be performed. Then depending on which types of movements are desired, the values in the objects are retrieved and sent to the correct output.

When the movements and/or motors are changed, such as when position is updated, it is important to overwrite the instance of that movement and/or motor in the handles.

Please check the example file below.

## Example ##

In this example is shown how to modify the movements.def and motors.def definition files in order to control a generic robotic hand with one DC motor per finger (flexion/extension) and two servo motors for managing the wrist rotation and flexion/extension.

### motors.def ###
The following motors.def file defines 7 motors labeled with alphabet letters, five of which are DC motors (control type is `0`, i.e. speed/PWM), while two are servo motors (control type is `1`, i.e. position).

    A,0,50
    B,0,50
    C,0,50
    D,0,50
    E,0,50
    F,1,70
    G,1,50

### movements.def ###
A long list of movements is defined in the following movements.def file. As an example, the movement #2 is labeled as "Close Hand" and, when issued, can either
 * move the #9 VRE degree of freedom towards direction 0, or
 * activate (according to the previous motors.def file) motors from A to E (i.e. all the fingers), all of them in the same direction (i.e. closing)

depending on the environment from which the command is issued.

    0, Rest, 0, 0, 0 0
    1, Open Hand, 9, 1, 1 1 2 1 3 1 4 1 5 1
    2, Close Hand, 9, 0, 1 0 2 0 3 0 4 0 5 0
    3, Flex Hand, 7, 0, 7 0
    4, Extend Hand, 7, 1, 7 1
    5, Pronation, 8, 0, 6 0
    6, Supination, 8, 1, 6 1
    7, Thumb Extend, 5, 1, 1 1 
    8, Thumb Flex, 5, 0, 1 0
    9, Index Extend, 4, 1, 2 1
    10, Index Flex, 4, 0, 2 0
    11, Middle Extend, 3, 1, 3 1
    12, Middle Flex, 3, 0, 3 0
    13, Ring Extend, 2, 1, 4 1
    14, Ring Flex, 2, 0, 4 0
    15, Little Extend, 1, 1, 5 1
    16, Little Flex, 1, 0, 5 0
    17, Point,10, 0, 1 0 2 1 3 0 4 0 5 0
    18, Thumb Yaw Flex, 6, 0, 0 0
    19, Thumb Yaw Extend, 6, 1, 0 0
    20, Agree, 0, 0, 1 1 2 0 3 0 4 0 5 0
    

# Functions roadmap #
**GUI_TestPatRec_Mov2Mov**
  * Load\_patRec
    * InitMotors
      * motor
    * InitMovements
      * movements
    * InitSensors
      * sensors
  * pb_selectctrlfolder_Callback (when pressing the "Select Folder" button)
    * InitMotors
      * motor
    * InitMovements
      * movements
  * pb_sensors_Callback (when pressing the "Sensors check" button)
   * InitSensors (GUI_Sensors opening)
     * sensors

# Notes #

  * In order to use the object-oriented way the last parameter of the _Load\_patRec_ function must be set to 1. This will load the names of the created movements into any dropdown menu with a given id of 'pm\_m?', where ? is a value from 1 to 20, in the new GUI being loaded.

  * There is another GUI for testing the realtime patrec which is not using neither the movements or motors objects (GUI\_TestPatRec\_Mov2MotDir). This relays in hard-coded relationships between the movements index and the assigned motor direction in the drop lists. Since we haven't used this GUI for a while, the realtime routines might need to be updated to the ones used in the GUI\_TestPatRec\_Mov2Mov.