The quality of the recorded myoelectric signals depends strongly on the following:

  * Bio-electrical amplifiers
  * Electrodes and their placements

# Electrode placement for sEMG #

Recording electrodes:

  * A bipolar configuration is preferable to reduce noise
  * Along the muscle fibers
  * Away from innervation zone if bipolar electrode. No applicable for monopolar
  * Away from the tendon
  * Away from muscle edge

Reference electrode:
  * As far as possible from the recording electrodes
  * Place in an electrically neutral tissue (bony parts)
  * Good electrical contact

NOTE: Do NOT forget to use a reference electrode!!!

# References #

For a very quick overview on sEMG see:

> [De Luca, C., Surface Electromyography: Detection and Recording: DelSys; 2002.](http://www.delsys.com/Attachments_pdf/WP_SEMGintro.pdf)

> Didactic materials freely available at [Delsys Knowledge Center](http://www.delsys.com/KnowledgeCenter/KnowledgeCenter.html)

> See also the recomendation of [SENIAM](http://www.seniam.org/).

For a deeper understanding see:

> Merletti, R. & Parker, P., **Electromyography: Physiology, Engineering, and Noninvasive Applications**, New Jersey, USA: John Wiley & Sons; 2004

> Barbero, M., Merletti, R., & Rainoldi, A., **Atlas of Muscle Innervation Zones: Understanding Surface Electromyography and Its Applications**, Springer; 2012.