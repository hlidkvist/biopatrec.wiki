[recSession](recSession.md) -> [sigTreated](sigTreated.md) -> [sigFeatures](sigFeatures.md) -> [patRec](patRec.md) ->
[motionTest](motionTest.md) || [tacTest](tacTest.md)

# tacTest #

  * sF (sampling frequency)
  * tW (time window)
  * trial(number of examination)
  * nR(number of repetition)
  * timeOut (time in which trial must be completed)
  * combination (combination of movement)
  * trialResult (final trial results)
  * ssTracker(tracker to keep track of the state space position)


see [TAC\_Test](TAC_Test.md) for more information.