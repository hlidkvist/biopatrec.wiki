# Popular “Science” on Artificial Limbs / Bionic Limbs / Robotic Prostheses #

If you are looking at this page as an introduction to the field, or just for curiosity, please be aware that the content of these articles has not been scientifically reviewed, and therefore all information provided by the authors must be taken with caution and not as the absolute truth. Of course, we cannot generalize completely, as few of them are accurate (not the majority though).

It is probably not a surprise that the media tend to exaggerate and raise false expectations on what science and technology can currently offer. At the same time, there is no doubt that the amount and quality of the work done by the scientists and engineers featured in these videos deserves recognition and respect. Unfortunately, their work is often misinterpreted by some “journalist” that feel that without exaggerated promises of futuristic achievements, it is not interesting enough to be published. With that being said, it is our responsibility as a community of professionals to share and comment on these articles, so please feel free to do so! 


## Quick and true facts: ##

  * We don’t really have the technology today to fully replace the functionality of a biological limb, thus we are not really close to create a “Robocop”, and definitely there is no need to worry about the threat of “Terminator” anytime soon.

  * The term "bionic" is often misused and therefore now ambiguous. There was a time when it meant an augmentation to the biological capabilities (stronger, faster, more accurate, etc...). Although originally was more on the side of biologically inspired mechatronics. Anyway, the fact is that if we cannot fully replace the biological limbs, we can hardly augmented them, as an exoskeleton could do for instance.

  * The media often use the phrase "mind controlled" or "thought controlled". This is at some extend true, but only because the brain basically controls everything we do! However, this might give the false impression that the prosthesis is controlled by simple "imagining" or "thinking", without any other muscular response, whereas in reality these signals are often recorded from muscles or the motor cortex rather than in any other area of the brain where "imagination" can take place (e.g. prefrontal cortex).

  * Most of the advanced technology presented by the media has only been used in controlled environments (research labs), and is it not really close to be a clinical reality for patients.

  * Lower limb prosthetics currently do a better job restoring functionality than upper limb prosthetics.

  * We have progressed tremendously on the hardware and software of robotic arms; we are now struggling on how to control them. As today, we CANNOT really do it fully in a way that is natural and intuitive OUTSIDE of controlled environments. We can do it with limited degrees of freedom within the lab and in short periods of time. Sadly, most of the media uses this to insinuate that patients are doing it in their daily activities as well, which is innacuarate.

  * We have rather advanced prosthetic devices since at least a decade, and yet, most of the patients do not use robotic/powered prostheses at all! That is probably the greatest indicator that most of the sophisticated prosthetics we see in the media, can only be used in the lab.

  * Although the previous points are rather pessimistic, there are a lot of intelligent and dedicated people trying to improve robotic prostheses, so we'll get there!

## Media on artificial limbs ##
**Control and Sensory Feedback**
  * [Beyond bionics: how the future of prosthetics is redefining humanity](https://youtu.be/GgTwa3CPrIE) by The Guardian, Jun 2018 (Neural interfacing for prosthetic limb are mostly peripheral rather than central).
  * [This Mind-Controlled Bionic Arm Can Touch and Feel](https://youtu.be/F_brnKz_2tI) by Motherboard, Aug 2016 (the claims of "first" are not actually true).
  * [Bionic arm: A pioneering union between man and machine - The Cure](https://youtu.be/7_lvVgth_ec) by Aljazeera, Jul 2016.
  * [Creating a Prosthetic Hand That Can Feel](http://spectrum.ieee.org/biomedical/bionics/creating-a-prosthetic-hand-that-can-feel) by IEEE Spectrum, Apr 2016.
  * [Behind the secret life of materials - Bionic Bodies](https://youtu.be/6v6S5wZAAig?list=PLmQdDYjAqXUnAELE2EPNcDtuUA1lK-QVI) by Materials Future, Sep 2015.
  * [Bionic Limbs](https://youtu.be/JvzF-2QyuDY) by Materials Future, Jul 2015.
  * [An artificial leg which can feel and reduce pain](http://www.euronews.com/2015/06/11/an-artificial-leg-which-can-feel-and-reduce-pain/) by Euronews, Jun 2015.
  * [Austrian men become first in the world to use ground-breaking bionic hands](https://youtu.be/OxbAliBxuOE) by Euronews, Feb 2015.
  * [Prosthetic Limbs, Controlled by Thought](http://www.nytimes.com/2015/05/21/technology/a-bionic-approach-to-prosthetics-controlled-by-thought.html?ref=technology&_r=1) by The New York Times, May 2014.
  * [Can modern prosthetics actually help reclaim the sense of touch?](http://youtu.be/16dlxeDxuWM) by PBS, Feb 2015.
  * [The Chuck Yeager of advanced arm prosthetics](http://www.pbs.org/newshour/updates/chuck-yeager-advanced-arm-prosthetics/) by PBS, Feb 2015.
  * [Mind-Controlled Prosthetic Arm Tested in Sweden](http://www.newsweek.com/mind-controlled-prosthetic-arm-tested-sweden-276281) by Newsweek, Oct 2014.
  * [Artificial arms get closer to the real thing](http://www.nature.com/news/artificial-arms-get-closer-to-the-real-thing-1.16111) by Nature News, Oct 2014.
  * [Marine is first to recieve experimental prosthetic control system](http://www.armytimes.com/videonetwork/3167215175001/Marine-is-First-to-Receive-Experimental-Prosthetic-Control-System) by Military Times, 2014.
  * [Can a prosthetic limb feel?](http://www.npr.org/2014/03/07/283458260/can-a-prosthetic-limb-feel) by TED hour, Mar 2014.
  * [Robotic Drumming Prosthesis](https://www.youtube.com/watch?v=io-jtlPv7y4) by Georgia Tech, Mar 2014.
  * [Bionic hand allows patient to 'feel'](http://www.bbc.com/news/health-26036429) by BBC, 5 Feb 2014.
  * [Restoration of natural sensation](https://www.youtube.com/watch?v=n9wyyuxI9Y8) by Cleveland VAMC and CWRU, Dec 2013.
  * [How the business of bionics is changing lives](http://www.cnbc.com/id/101233869) by CNBC in Nov 2013.
  * [Touching me, Touching you](https://soundcloud.com/sciencelife/november-2013-touching-me) by Science Life, Nov 2013.
  * [Bionic leg helps shark-attack victim walk](http://www.cnn.com/2013/10/02/tech/innovation/bionic-leg-orig-ideas/) by CNN in Oct 2013.
  * [Chicago Doctors Unveil First Thought-Controlled Bionic Leg](http://www.nbcchicago.com/news/local/Chicago-Doctors-Unveil-First-Thought-Controlled-Bionic-Leg-225250672.html) by NBC and [NewScientists](http://www.newscientist.com/article/dn24265-man-controls-new-prosthetic-leg-using-thought-alone.html#.UlFY31BmjTo) in Sep 2013.
  * [Thought-controlled prosthetics](http://www.scienceupdate.com/2012/12/arm/) by AAAS in Dec 2012.
  * [Can you build a bionic body? The arm](http://www.bbc.co.uk/news/health-17139965) by BBC News in Mar 2012.
  * [Hugh Herr, Bionic Man - Part 1](http://vimeo.com/43486048#) by The Next List in Jun 2012.
  * [Soldier Andrew Garthwaite to get bionic arm controlled by mind](http://www.bbc.co.uk/news/uk-england-tyne-16746496) by BBC News in Jan 2012.
  * [Thought-controlled devices a step closer](http://www.bbc.co.uk/news/science-environment-12499818) By BBC in Feb 2011.
  * [Man controls robotic hand with mind](http://news.discovery.com/tech/videos/tech-man-controls-robotic-hand-with-mind.htm) by Discovery Channel in Dec 2010.
  * ['60 Minutes': Revolutionizing prosthetics](http://cnettv.cnet.com/2001-1_53-50005779.html) by CBS News sometime before 2009.
  * [Cutting edge prosthetic arms](http://www.youtube.com/watch?v=T6R5bm6qx2E) by Pentagon Channel sometime before May 2008.

**Hardware**
  * [The Pentagon's Luke Skywalker-inspired bionic arm is FINALLY ready](http://www.dailymail.co.uk/sciencetech/article-4069290/The-Pentagon-s-Luke-Skywalker-inspired-bionic-arm-FINALLY-ready-DARPA-begins-fitting-veterans-prosthetic-limb-powered-muscle-twitches.html) by Mail online, Dec 2016.
  * [Amputee Makes History with APL’s Modular Prosthetic Limb](https://www.youtube.com/watch?v=9NOncx2jU0Q) by JHU, Dec 2014.
  * [Darpa's super-dexterous robot arm gets FDA approval](http://www.wired.co.uk/news/archive/2014-05/12/darpa-robotic-limb-fda-approval) by Wired UK, May 2014.
  * [How its Made Artificial Limbs (socket attachment)](https://www.youtube.com/watch?v=y1T9sS4A7To) by How Its Made, 21 Jan 2014.
  * [Robotic arm grasps the future](http://edition.cnn.com/video/#/video/international/2013/05/02/art-movement-michael-mcloughlin-robotic-arm.cnn) by CNN in May 2013.
  * [Modular Prosthetic Limb](http://www.youtube.com/watch?v=3HsccKwoeOM&feature=related) by JHU/APL sometime before Feb 2011.
  * [Dean Kamen's Robotic "Luke" Arm](http://youtu.be/R0_mLumx-6Y) by Spectrum Magazine in 2008.

**Hardware on budget**
  * [Low-cost 3D-printed prosthetic hand to be tested on amputees in Ecuador](http://www.gizmag.com/3d-printed-prosthetic-hand/34615/?utm_source=Gizmag+Subscribers&utm_campaign=30149df3b2-UA-2235360-4&utm_medium=email&utm_term=0_65b67362bd-30149df3b2-76705984) by Gizmag in Nov 2014.
  * [The Boy and the Bionic Hand](http://www.gereports.com/post/93402305860/the-boy-and-the-bionic-hand-a-chance-hospital) by GE in Aug 2014.
  * [Inexpensive home-brewed prostheses created using 3D printers](http://www.gizmag.com/inexpensive-prostheses/26009/) by Gizmag in Jan 2013.

**Alternatives**
  * [Luke Skywalker’s bionic hand made possible by ultrasound technology](http://www.news.gatech.edu/2017/12/11/force-strong-amputee-controls-individual-prosthetic-fingers) by Gorgia Tech, Dec 2017
  * [7 Finger Robot](http://youtu.be/FTJW5YSRZhw) by MIT July 2014.

## Media on commercially available prostheses ##

  * [iLimb Digitis](http://youtu.be/uf0IeSwZRGA) by Digital Trends Mar 2014.
  * [The bionic hand with the human touch](http://edition.cnn.com/video/?/video/business/2013/01/31/make-create-innovate-i-limb-prosthetics-will-am-britney-spears.cnn#/video/business/2013/01/31/make-create-innovate-i-limb-prosthetics-will-am-britney-spears.cnn) by CNN in Jan 2013.
  * [Amputee Patrick demonstrates his new bionic hand](http://www.bbc.co.uk/news/health-13378036) by BBC news in May 2011.

## Media related BioPatRec ##

  * [Why Amputees Get Virtual Reality Limbs](https://www.youtube.com/watch?v=tRmYNwWRR78) by Dnews in Mar 2014.
  * [Phantom menace: augmented reality eases missing limb pain](https://theconversation.com/phantom-menace-augmented-reality-eases-missing-limb-pain-23479) by The Conversention in Feb 2014.
  * [Virtual arm eases phantom limb pain](http://www.bbc.co.uk/news/health-26327457) by BBC in Feb 2014.
  * [Virtual Reality Gives Amputee Real-Life Relief](http://abcnews.go.com/blogs/health/2014/02/26/virtual-reality-gives-amputee-real-life-relief/) by ABC in Feb 2014.
  * Other languages
    * [Futuremag #16](http://www.futuremag.fr/emission/emission-16) by ARTE May 2014 (French, starts 08:20)
    * [Futuremag #16](http://www.futuremag.de/sendung/sendung-16) by ARTE May 2014 (German, starts 08:20)

## Talks and Academic Lectures ##

Keep in mind that presenters would mostly focused on their group's work, and of course, their own approach to the different problems.

#### Control ####
  * [Biomechatronic limbs and neurorehabilitation of pain](https://youtu.be/b4zQCSnAi1w) by Max Ortiz Catalan at Monash University, Sep 2017.
  * [Bionic limbs integrated to bone, nerves, and muscles](http://youtu.be/V4UQU4392wM) by Max Ortiz Catalan at TEDx Göteborg, Nov 2014.
  * [Targeted Muscle Reinnervation](https://www.youtube.com/watch?v=6J_I9NlkHJc) by Pedro Irazoqui at nanoHUB.org, Apr 2014.
  * [The new bionics that let us run, climb and dance](http://www.ted.com/talks/hugh_herr_the_new_bionics_that_let_us_run_climb_and_dance#t-534507) by Hugh Herr at TED,  Mar 2014.
  * [Evolving prosthetics](https://youtu.be/B44gP70tJ7w) by Kevin Englehart at TEDx UNB, Sep 2013.
  * [A prosthetic arm that "feels"](http://www.youtube.com/watch?v=MLvwTlbj1Y8) by Todd Kuiken at TED, Jul 2011.
  * [Bionic Being: The New Prosthetics](http://youtu.be/3dGgSuaQg1s) by Shawn Kelly at Carnegie Mellon University, Jun 2011.
  * [Robotics: New Possibilities in Prosthetic Limbs](http://www.youtube.com/watch?v=y_NGycsdW5o) by Michael Goldfarb at Vanderbilt University, 2010.

#### Hardware ####
  * [Without a leg to stand on - 3D printing prosthetics](https://youtu.be/LNohxpJntZo) by Matt Ratto at TEDx UofT, Mar 2014.
  * [Life enhancing prosthetics - 3D printed and open sourced](https://youtu.be/peoZJRtnPiA) by Ivan Owen at TEDx Rainier, Jan 2014.
  * [Developing world's lightest ever prosthetic hand](https://youtu.be/75wpABkscHU) by Gerwin Smit at TEDx Delft, Dec 2013.
  * [Unconventional prostheses](https://youtu.be/_4j_KmXN2Ss) by Össur Kristinsson at TEDx Reykjavik, Jul 2013. 
  * [A helping hand with prosthetics](https://www.youtube.com/watch?v=jaRNbgRK8k4) by Joel Gibbard at TEDx Exeter, Apr 2013.
  * [The sore problem of prosthetic limbs](http://www.ted.com/talks/david_sengeh_the_sore_problem_of_prosthetic_limbs) by David Sengeh at TED, Mar 2014.
  * [The $80 prosthetic knee that's changing lives](http://www.ted.com/talks/krista_donaldson_the_80_prosthetic_knee_that_s_changing_lives) by Krista Donaldson at TED Women, Dec 2013.
  * [New prosthetic arm for veterans](http://www.youtube.com/watch?v=RiJzJ771vDw) by Dean Kamen at TED, Mar 2007.

#### Design ####
  * [Prosthetic limbs as surrealist art](https://youtu.be/RLwCrMKfouo) by Sophie de Oliveira Barata at TEDx Brixton, Oct 2013. 
  * [Beautiful artificial limbs](https://youtu.be/fir5HI0Gwrc) by Scott Summit at TED, 2011.

#### Neural Engineering ####
  * [Neural Interfaces and Neural Dust](https://youtu.be/SNuNz451NSM) by Michel Maharbiz at U California, Aug 2016. 
  * [Brain Control of Prosthetic Devices](https://youtu.be/NSU_MTA62Bc) by J. Carmena and M. Maharbiz at The Road AHead, Jan 2015.
  * [Neural prosthetics](https://youtu.be/ZuATvhlcUU4) by Krishna Shenoy at TEDxStanford, Jun 2013.


#### Inspirational ####
  * [Baby charlotte Grows](http://attitudelive.com/documentary/baby-charlotte-grows) by AttitudeLive, 2015.
  * [The aesthetics of prosthetics](https://youtu.be/uCZAmTAveII) by Kevin Connolly at TEDxBozeman, Apr 2012.
  * [Living beyond limits](https://www.youtube.com/watch?v=N2QZM7azGoA&sns%3Dem) by Amy Purdy at TED, Jun 2011.
  * [Changing my legs - and my mindset](http://www.ted.com/talks/aimee_mullins_on_running) by Aimee Mullins at TED, Feb 1998.