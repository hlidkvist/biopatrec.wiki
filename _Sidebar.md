  * [Home](https://github.com/biopatrec/biopatrec/wiki)
  * [Highlights](BioPatRec_Highlights.md)
  * [Startup Guide](BioPatRec_StartupGuide.md)
  * [Requirements](BioPatRec_Requirements.md)
  * [Source Code](SourceCode.md)
  * [Data Repository](Data_Repository.md)
  * [Wiki content](Wiki_content.md)
  * [Functions roadmap](BioPatRec_Roadmap.md)
    * [RoadMap\_FRA](RoadMap_FRA.md)
    * [RoadMap\_TRE](RoadMap_TRE.md)
    * [RoadMap\_TVA](RoadMap_TVA.md)
  * [BioPatRec](BioPatRec.md)
    * [Comm](Comm.md)
    * [SigRecordings](SigRecordings.md)
    * [SigTreatment](SigTreatment.md)
    * [SigFeatures](SigFeatures.md)
    * [PatRec](PatRec.md)
    * [Control](Control.md)
    * [DataAnalysis](DataAnalysis.md)
  * Data structures:
    * [recSession](recSession.md)
    * [sigTreated](sigTreated.md)
    * [sigFeatures](sigFeaturesStruct.md)
    * [patRec](patRecStruct.md)
    * [motionTest](motionTestStruct.md)
    * [tacTest](tacTestStruct.md)
  * Data sets: the [xSets](xSets.md)
  * [Coding Standard](Coding_Standard.md)
  * [Coding Tips](Coding_Tips.md)
  * [Troubleshooting](Troubleshooting.md)
  * [Important coding changes](Important_coding_changes.md)
  * [Who's in?](Collaborators.md)
    * Projects
      * [NTNU](NTNU.md)
      * [SSSA/PrensiliaSRL](SSSA_PrensiliaSRL.md)
      * [UALR](UALR.md)
  * [Related projects](OS_projects.md)
  * [Popular Science](PopularScience.md)