You can browse the source code using the "[<>](https://github.com/biopatrec/biopatrec/)" link on the right side of this page (source code). The source code is available by cloning the entire repository (not recommended) or the released versions under "tags" (recommended). You can also use an SVN client such as [Tortoise SVN](http://tortoisesvn.tigris.org/) with the checkout addresses listed below (highly recommended option, see step-by-step instructions below).

Each release of BioPatRec is available in a distinct tag, and the data repository is available separably in the branch [Data_Repository](https://github.com/biopatrec/biopatrec/tree/Data_Repository). This was done in order to keep a decent size for each release.

Please be aware that we do not use GitHub in the conventional way (tracking changes), but mostly to provide the released/tags of the code and documentation.

# BioPatRec Source Code #

The description of each release is available in [BioPatRec Highlights](BioPatRec_Highlights.md)

 * [BioPatRec FRA](https://github.com/biopatrec/biopatrec/tree/v4.0) (v4.0), [ZIP/TAR](https://github.com/biopatrec/biopatrec/releases/tag/v4.0).
  * SVN checkout address: https://github.com/biopatrec/biopatrec/tags/v4.0

 * [BioPatRec TRE](https://github.com/biopatrec/biopatrec/tree/v3.0) (v3.0), [ZIP/TAR](https://github.com/biopatrec/biopatrec/releases/tag/v3.0).
  * SVN checkout address: https://github.com/biopatrec/biopatrec/tags/v3.0

 * [BioPatRec TVÅ](https://github.com/biopatrec/biopatrec/tree/v2.0/BioPatRec_TVA) (v2.0), [ZIP/TAR](https://github.com/biopatrec/biopatrec/releases/tag/v2.0), and [reference article](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6744619).
  * SVN checkout address: https://github.com/biopatrec/biopatrec/tags/v2.0/BioPatRec_TVA

 * [BioPatRec ETT](https://github.com/biopatrec/biopatrec/tree/v1.0/BioPatRec_ETT) (v1.0), [ZIP/TAR](https://github.com/biopatrec/biopatrec/releases/tag/v1.0), and [reference article](http://www.scfbm.org/content/8/1/11/)
  * SVN checkout address: https://github.com/biopatrec/biopatrec/tags/v1.0/BioPatRec_ETT

# EMG Repository #

The data repository is available at the branch "[Data_Repository](https://github.com/biopatrec/biopatrec/tree/Data_Repository)" and each set is described in the [Data Repository](Data_Repository.md) page.

 * [6mov8chFUS](https://github.com/biopatrec/biopatrec/tree/Data_Repository/6mov8chFUS) and [reference article](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6744619).
  * SVN checkout address: https://github.com/biopatrec/biopatrec/branches/Data_Repository/6mov8chFUS

 * [10mov4chForearmUntargeted](https://github.com/biopatrec/biopatrec/tree/Data_Repository/10mov4chForearmUntargeted) and [reference article](http://www.scfbm.org/content/8/1/11/).
  * SVN checkout address: https://github.com/biopatrec/biopatrec/branches/Data_Repository/10mov4chForearmUntargeted

 * [6mov8chFUS_MLP_Th](https://github.com/biopatrec/biopatrec/tree/Data_Repository/6mov8chFUS_MLP_Th) and [reference article](http://publications.lib.chalmers.se/records/fulltext/176132/local_176132.pdf).
   * SVN checkout address: https://github.com/biopatrec/biopatrec/branches/Data_Repository/6mov8chFUS_MLP_Th

 * [6mov8chFUS_MLP_Topologies](https://github.com/biopatrec/biopatrec/tree/Data_Repository/6mov8chFUS_MLP_Topologies) and [reference article](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6611081&isnumber=6609410).
   * SVN checkout address: https://github.com/biopatrec/biopatrec/branches/Data_Repository/6mov8chFUS_MLP_Topologies

## Step-by-Step instructions using Tortoise SVN
 * Download and install _TortoiseSVN_
 * Create a new folder in the desired directory of your computer
 * Right click on the folder and click on _SVN Checkout_ in the dropdown menu, a _Checkout_ GUI will open
 * Paste the SVN checkout address of what you want to download under _URL of repository_.  
 * Click ok

  

