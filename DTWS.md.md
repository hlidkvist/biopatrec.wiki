# Delsys Trigno Wireless System #

A multimodal system is a system consisting of more than one type of sensor. A multimodal unit (MMU) is a unit that by itself is a multimodal system. The Delsys Trigno Wireless System consists of sensors that has surface electromyography (SEMG) and a 3-axis accelerometer.

The Delsys Trigno Wireless System has a base station and 16 Trigno sensors that can communicate wirelessly with the base station. Ecery sensor has 4 channels. One channel for the SEMG and three channels for the accelerometer.

This list contains the most the most important features of the System.
  * 16 EMG channels.
  * 48 acceleromter channels.
  * 64 channels real-time analog output.
  * Full USB support
  * All sensor are Wireless
  * All sensors has 16 bit signal resoltion at 2000Hz.

At [NTNU](NTNU.md) this system is used with USB. For this setup to work a SDK that is provided with the complete system has to be used in order to access the data stream from the sensor system.

For more information on the system see: http://www.delsys.com/products/trignowireless.html