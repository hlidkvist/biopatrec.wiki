These are few links to other open source projects related to limb prosthetics

# General #

  * [Open Prosthetics](http://openprosthetics.org)

# Control #

  * [Myoelectric Control (MECLab)](http://www.sce.carleton.ca/faculty/chan/index.php?page=matlab)

# Hardware #

  * [The Handi Hand - Blinc](https://blincdev.ca/the-handi-hand/overview/)
  * [The Bento Arm - Blinc](https://blincdev.ca/the-bento-arm/overview/)
  * [The Open Hand Project](http://www.openhandproject.org)
  * [e-NABLE](http://enablingthefuture.org)
  * [Open Bionics (COM)](http://www.openbionics.com)
  * [Open Bionics (ORG)](http://www.openbionics.org/)
  * [Open Source Hand](http://opensourcehand.wordpress.com/)
  * [Wrist Quick Disconnect](https://www.dropbox.com/sh/hwy7ayc3t47g6i7/AADvk3FDkp9xKv0K514O5eHda?dl=0)
  * [The Prosthetic Device Communication Protocol (PDCP)](http://www.unb.ca/research/institutes/biomedical/pdcp.html)
  * [Prosthetics for Kids as a Creative and Social Tool](https://youtu.be/e0ne-XfXtnQ)
  * [PSYONIC: advanced open source 3D printed hand](http://www.psyonic.co/#home-psyonic)

# Data #

  * [Ninaweb](http://ninapro.hevs.ch/) (sEMG and kinematic data of hand movements)