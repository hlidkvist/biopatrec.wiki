# BioPatRec and Sensors #

By introducing the control framework in BioPatRec TRE it is now possible to read sensors from a controlled device (such as fingertip force sensors, or motors current sensors). 

NOTES:
 * In order to learn how to control motors of your device through BioPatRec, please check the [Movements\_Protocol](Movements_Protocol.md) page
 * The device control and the sensors reading can possibly work together on real time. Please note that since the reading function is based on a timer defining the sensors acquisition frequency, you might need to properly set the frequency depending on the hardware speed.


## Protocol ##

The protocol is based on the sensor object that is created when the "Sensor Check" button is pressed. The object is filled accordingly to the values written in the sensors.def file, similarly to what happens with the Movements and Motors definition files (and as described in the [Movements\_Protocol](Movements_Protocol.md) page).

### Sensor Object ###

The sensors information are directly read from the definition file where each line represents a sensor, the syntax is shown in the following table:

\#1 | \#2 | 
--- | --- | 
id  | name | 

The id is the identifier that will be used during the communication with the device, while the name is just a label that will be used in the GUI_Sensors GUI, as explained below. Each sensor object is then created using the sensor class (Sensor.m).

### GUI_Sensors ###

Every sensor defined in the sensors.def file will be listed in the GUI_Sensors using the name chosen in the file. In order to start collecting sensors information, it is only required to press the "Start" button. The output of all the defined sensors will be shown in the graph, each of which shifted and scaled vertically in order to avoid overlays. If BioPatRec cannot find the sensors.def file, the Start and Stop buttons will be disabled automatically.

More in detail, once opened, the GUI initializes the sensors objects by calling the InitSensors function, then configures a timer (by default set to 0.05 seconds) to periodically call the SensorRequest function, which is in charge of getting the values and plotting them in the graph. In fact, the SensorRequest function calls the ReadSensors function, in which the communication protocol is implemented. The proposed communication protocol for sensors readings is shown in the following table:

order | byte        | definition                                                             |
----- | ----------- | ---------------------------------------------------------------------- |
1     |  S (0x53)   | header used to identify the sensor-message in the firmware             |
2     | id          | the unique id defined in the sensors.def file used to identify the sensor to be read |

The response message from the firmware is structured as follows:

order | byte        | definition                                          |
----- | ----------- | --------------------------------------------------- |
1     | id          | the same id of the sensor requested to the hardware |
2     | value       | the requested sensor reading                        |


### Example ###
The following sensors.def file defines 4 sensors, with ascending ids (used during the communication with the device). The first two are analog inputs, while the others are digital switches. Remember that the label is only used for graphical purposes.

    0, AN1
    1, AN2
    2, SW1
    3, SW2

## What if... ##
If you cannot modify the embedded software of the device you are aiming to use (or if you don't like the suggested protocol), you will need to write (among others) your custom ReadSensors function accordingly to the specific communication protocol used by that device. For a comprehensive guide on how to do that, please check the tutorial on [how to add a prosthetic device to BioPatRec](how to add prosthetic devices.md).