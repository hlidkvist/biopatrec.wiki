# Motors communication protocol #
By introducing the control framework in BioPatRec TRE, the communication protocol used to drive motors was changed in order to have a more standardized and flexible way of communicating.

## Motor command ##
The core of the communication is actually a single 5-bytes command, called _motor command_, and structured as follows:

order | byte        | definition                                                             |
----- | ----------- | ---------------------------------------------------------------------- |
1     | 'M' (0x4D)  | header used to identify the motor command in the firmware              |
2     | type        | used to identify the control type                                      |
3     | id          | the unique id used to identify the motor                               |
4     | direction   | used to specify the direction of movement of the motor                 |
5     | pos/speed   | the control value (position or PWM depending on the control type)      |

Currently, only two control types are supported in BioPatRec: PWM/speed control (`type=0`, suitable for DC motors) and position control (`type=1`, suitable for servo motors). Thus, depending on the control type, the last byte of the motor command is either the desired PWM or desired position.

The protocol expects that a single-byte echo message is sent back from the device ensuring that the communication worked properly. The echo message consists of the id sent as the third byte of the motor command.

The above command is implemented in the SendMotorCommand function. In particular, when a new movement is requested, SendMotorCommand is called (by the MotorsOn function) for every motor taking part in that movement (as defined in the movements.def file). E.g., if a specific movement is based on two motors (as, for instance, a bi-digital grasp), the activation of those motors will require two motor commands, and indeed the SendMotorCommand is called twice. This can be surely optimized for faster performance by modifying directly the MotorsOn and MotorsOff functions located in the Control folder.

## Test connection command ##
An additional command is provided in order to quickly test if the communication between the controlled device and BioPatRec is working properly. As shown below, this is a simple two-bytes command:

order | byte        | definition         |
----- | ----------- | ------------------ |
1     | 'A' (0x41)  | first header byte  |
2     | 'C' (0x43)  | second header byte |

The echo message that the device is expected to return is a single-byte message containing a 'C' (0x43). This command is implemented in the TestConnection function located in the Control folder and is used every time that the Test button in the GUI_TestPatRec_Mov2Mov is pressed.

## What if... ##
If you cannot modify the embedded software of the device you are aiming to use (or if you don't like the suggested protocol), you will need to write (among others) your custom SendMotorCommand and TestConnection functions accordingly to the specific communication protocol used by that device. For a comprehensive guide on how to do that, please check the tutorial on [how to add a prosthetic device to BioPatRec](how to add prosthetic devices.md).

NOTES:
 * If your device is provided of sensors and you aim to interface with those, then you should have a look on [sensors protocol](sensors protocol.md) page as well.


# Functions Roadmap #
  * MotorsOn 
    * SendMotorCommand (called for every motor associated to the requested movement)
  * MotorsOff
    * if motor_type = DC -> SendMotorCommand (with ctrl_val = 0)
    * if motor_type = servo -> nothing
