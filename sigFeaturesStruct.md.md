[recSession](recSession.md) -> [sigTreated](sigTreated.md) -> [sigFeatures](sigFeatures.md) -> [patRec](patRec.md)

# sigFeatures #
  * sF (sampling frequency)
  * tW (time window)
  * nCh (number of channels)
  * mov (movements)
  * scaled
  * noise
  * dev (device used for recordings)
  * comm
  * fFilter (frequency filter)
  * sFilter (spatial filter)
  * trSets (training sets, number)
  * vSets (validation sets, number)
  * tSets (testing sets, number)
  * sigSeparation 
  * wOverlap (window overlap) 
  * trFeatures (training features), see [xFeatures](xFeatures.md)
  * vFeatures (validation features), see [xFeatures](xFeatures.md)
  * tFeatures (test features), see [xFeatures](xFeatures.md)

This structure contains the signal features to be used for pattern recognition. These are the training, validation and testing sets (see [xSets](xSets.md) created in PatRec).