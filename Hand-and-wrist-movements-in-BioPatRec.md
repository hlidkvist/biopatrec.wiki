16 hand and wrist movements can be recorded.

The shortcuts in accordance with BioPatRec paper
# Open hand (OH)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Open%20Hand.JPG)
# Close hand (CH)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Close%20Hand.JPG)
# Flex hand (FH)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Flex%20Hand.JPG)
# Extend hand (EH)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Extend%20Hand.JPG)
# Pronation (PR)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Pronation.JPG)
# Supination (SP)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Supination.JPG)
# Side grip (SG)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Side%20Grip.JPG)
# Fine grip (FG)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Supination.JPG)
# Thumbs up (AG)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Agree.JPG)
# Pointer extension (PT)
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Pointer.JPG)
# Thumb extension
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Thumb%20Ext.JPG)
# Thumb flexion
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Thumb%20Flex.JPG)
# Thumb abduction
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Thumb%20Abduc.JPG)
# Thumb adduction
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Thumb%20Adduc.JPG)
# Ellbow flexion
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Flex%20Elbow.jpg)
# Ellbox extension
![](https://github.com/biopatrec/biopatrec/blob/master/Img/Extend%20Elbow.jpg)