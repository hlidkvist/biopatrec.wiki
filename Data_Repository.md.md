The recording session described below can be downloaded from the [data repository](https://github.com/biopatrec/biopatrec/tree/Data_Repository) branch. We recommend placing the folders containing these files into a folder called "Data repository" within the BioPatRec root folder.

This is a repository of myoelectric recording sessions ([recSession](recSession.md)) useful for development, as well as for algorithms benchmarking on common data sets.

Remember that even if you are using the same data to compare different algorithms, small variations in signal processing and training settings can make a difference on the final results.

##[10mov4chUntargetedForearm](https://github.com/biopatrec/biopatrec/tree/Data_Repository/10mov4chForearmUntargeted)##
  * 10 hand and wrist motions
    * Hand Open/Close
    * Wrist Flex/Extend
    * Pro/Supination
    * Fine/Side Grip
    * Pointer (index extension)
    * Agree or Thumb up
  * 3 seconds contraction time with 3 seconds for relaxation between each repetition
  * 3 repetitions of each motion
  * 4 bipolar electrodes (Disposable Ag/AgCl)
  * 1 cm electrode diameter
  * 2 cm inter-electrode distance for the bipole
  * Electrodes were equally spaced around the most proximal third of the forearm.
  * The first channel was always placed along the extensor carpi ulnaris
  * The proximal electrode of each bipole was always connected to the positive terminal of the BioAmp.
  * 17 subjects ([original study](http://www.scfbm.org/content/8/1/11))
  * 20 subjects (final release)
  * Amplifiers:
    * In-house design (MyoAmpF2F4-VGI8)
    * CMRR > 130 dB
    * Gain 71 dB
    * Embedded active ﬁltering:
      * 4th order high-pass ﬁlter at 20 Hz
      * 2nd order low-pass ﬁlter at 400 Hz
      * Notch ﬁlter at 50 Hz.
    * Galvanic isolation rated to 1,500 Vrms
  * Signals were digitized at 2 kHz with a 14-bits resolution.

##[6mov8chUFS (Untargeted Forearm Simultaneous)](https://github.com/biopatrec/biopatrec/tree/Data_Repository/6mov8chFUS)##
  * 27 clases
    * 6 Individual hand and wrist motions
      * Hand Open/Close
      * Wrist Flex/Extend
      * Pro/Supination
    * 20 possible combinations of the motions above
    * 1 no motion / rest class
  * 3 seconds contraction time with 3 seconds for relaxation between each repetition
  * 3 repetitions of each motion
  * 8 bipolar electrodes (Disposable Ag/AgCl)
  * 1 cm electrode diameter
  * 2 cm inter-electrode distance for the bipole
  * Electrodes were equally spaced around the most proximal third of the forearm.
  * The first channel was always placed along the extensor carpi ulnaris
  * The proximal electrode of each bipole was always connected to the positive terminal of the BioAmp.
  * 17 subjects ([published study](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6744619))
  * Amplifiers:
    * In-house design (MyoAmpF2F4-VGI8)
    * CMRR > 130 dB
    * Gain 66 dB (2,000 linear)
    * Embedded active ﬁltering:
      * 4th order high-pass ﬁlter at 20 Hz
      * 2nd order low-pass ﬁlter at 400 Hz
      * Notch ﬁlter at 50 Hz.
    * Galvanic isolation rated to 1,500 Vrms
  * Signals were digitized at 2 kHz with a 16-bits resolution.

##[10mov4chUF_AFEs (Untargeted Forearm, AFEs study)](https://github.com/biopatrec/biopatrec/tree/Data_Repository/10mov4chFU_AFEs)##
 * ([published study](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7318805))
  * 8 subjects execute 10 movements with 3 different configurations: 
    * TI ADS1299 
    * TI ADS1299 with bias-driver enabled 
    * Intan RHA2216
  * 10 Individual hand and wrist motions
    * Hand Open/Close
    * Wrist Flex/Extend
    * Pro/Supination
    * Side Grip
    * Fine Grip
    * Agree
    * Pointer
  * 3 seconds contraction time with 3 seconds for relaxation between each repetition
  * 3 repetitions of each motion
  * 4 bipolar electrodes (Disposable Ag/AgCl)
  * 1 cm electrode diameter
  * 2 cm inter-electrode distance for the bipole
  * Electrodes were equally spaced around the most proximal third of the forearm.
  * The first channel was always placed along the extensor carpi ulnaris
  * The proximal electrode of each bipole was always connected to the positive terminal of the acquisition device.
  * For more info on amplifiers setup see the linked article.

##[8mov16chLowerLimb (Lower Limb recordings, study on electrode configurations)](https://github.com/biopatrec/biopatrec/tree/Data_Repository/8mov16chLowerLimb)##
  * recordings on 2 separate groups of 8 participants each
    * Targeted Monopolar Configuration (TMC) vs. Targeted Bipolar Configuration (TBC) 8 channels each
    * Untargeted Monopolar Configuration (UMC) with 16 channels
  * for each group 8 subjects execute 8 movements  
    * knee flexion/extension, 
    * ankle plantarflexion/dorsiflexion, 
    * hip rotation medial/lateral, 
    * tibial rotation medial/lateral
  * 4 seconds contraction time with 4 seconds for relaxation between each repetition
  * 3 repetitions of each motion
  * 2000 Hz sampling rate
  * acquisition system with 16 channels
  * for more info the electrode configurations, acquisition system and recording session see the linked article.