# Introduction

Support Vector Machine is a commonly used method for pattern recognition ([PatRec](\PatRec.md)). See [wikipedia link](https://en.wikipedia.org/wiki/Support_vector_machine).

# Implementation
It was implemented on MATLAB 2013b using _svmtrain_ and _svmclassify_, requiring the _Statistics_ and _Machine Learning_ Toolboxes. 

Both _svmtrain_ and _svmclassify_ might be removed in MATLAB new versions, so the code will have to be updated to _fitcsvm_ and _ClassificationSVM_ for future releases.

# Functions Roadmap
## Training
* SVMAnalysis
 * svmtrain
 * svmclassify

## Testing
*  SVMTest 
 * svmclassify
