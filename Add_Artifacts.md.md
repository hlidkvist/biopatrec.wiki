# Adding Artifacts

With this option, artifacts can be inserted into the trimmed EMG data in the pre-processing step. This option can be found in both the _GUI_SigTreatment_ for offline usage and in the _GUI_TestPatRec_Mov2Mov_ for real-time applications during a motion test (Editbox: _insert artifacts?_).

If the "Add Artifact" is selected, the following input dialog will be displayed:

![](/biopatrec/biopatrec/blob/wiki/img/AddArtifact.PNG)

**Mean time gap [s]:** 
If only 1 number is set, the artifacts are randomly inserted throughout all data sets (training, validation and testing). Input of the time intervalls in the following form: "t_tr, t_v, t_z" yields insertation of the artifacts with average time gaps within the trData, vData and tData sets accordingly to the three different time intervalls. If a time intervall is set to 0 no artifacts are included in the according data set.

**Number of Channels:**
The artifacts are inserted in 1 up to the defined number of channels at the randomly specified time instances. 

**Maximum Amplitude:**
The artifacts maximum amplitude is defined relative to the absolute mean value of the current time windows.

### Artifact Database
After the parameters have been set, the user is asked to choose an MATLAB struct (artifact database). If no file is selected, a default DB is used (\SigTreatment\Motion Filters\databases\default_artDB.mat). Creating an artifact database, can be done in the "GUI_Recordings" via _Select artifact to DB_ (Tools Menu). The selected artifacts are normalized (-1 to 1). They can be grouped into different categories: Rapid movement, cable movement and contact artifact and are finally saved in a MATLAB struct (xxx_artDB.mat) in current folder.