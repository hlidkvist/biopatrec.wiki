# Introduction

This is the standard statistical method for pattern recognition ([PatRec](\PatRec.md)), where Linear Discriminant Analysis ([LDA](https://en.wikipedia.org/wiki/Linear_discriminant_analysis)) is probably the most popular and well-known.
# Implementation
The implementation is based on MATLAB `classify `function. See [Disciminant Analysis in Matlab](http://www.mathworks.se/help/toolbox/stats/classify.html) for details.

All discriminant analysis types available in Matlab can also be selected from the GUI_PatRec.

* linear
* diaglinear
* quadratic
* diagquadratic
* mahalanobis 

Note: It requires the Matlab's Statistics Toolbox.
# Functions Roadmap
## Training
* **DiscriminantAnalysis**
* GetSetsLables_Stack
* classify 

## Testing
*  DiscriminantTest 