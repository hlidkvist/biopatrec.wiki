# Wavelet De-noising

This algorithm handles the problem of signal recovery from noisy data using wavelet analysis. The general concept of wavelet de-noising involves the four main steps:

![](/biopatrec/biopatrec/blob/wiki/img/WaveletDenoising.PNG)

* Wavelet decompostion (msWaveletDec.m)
* Thresholding selection (msWaveletThr.m)
* Coeeficient shrinking (msWaveletShrink.m)
* Signal reconstruction (msWaveletRec.m)

## Parameter Selection GUI
All necessary parameters can be selected easily via the "GUI_Denoising", which can be accessed via the GUI_Recordings and GUI_SignalTreatment.

![](/biopatrec/biopatrec/blob/wiki/img/GUI_Denoising.PNG)

**Wavelet transform:**
* Type of wavelet transform (stationary/discrete)
* Wavelet basis function (db, sym, coif...)
  (Filter coefficients are calculated by MATLAB Wavelet Toolbox function _wfilters_)
* Level decompostion depth
* Window extension (can be used to reduce border distortion effects)
* Circle spinning (circular shifting of the signal and subsequent averaging)
 
**Thresholding**
* Shrinking types:
![](/biopatrec/biopatrec/blob/wiki/img/WaveletShrinkMath.PNG)
* Thresholding selection rule
* Rescaling methods
* Set Factor to manually correct threshold values by multiplication
* keep Approximation (if unselected, approximation coefficients are also shrinked by threshholding)
* Wiener Correction: Performs simplified wavelet wiener filtering:
![](/biopatrec/biopatrec/blob/wiki/img/WaveletWienerFiltering.PNG)
The Wiener Correction Factor (WCF) is hereby defined as follows:
![](/biopatrec/biopatrec/blob/wiki/img/WCF.PNG)
