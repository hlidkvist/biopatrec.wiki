NOTE: This is an ongoing development and therefore the documentation is not yet finalized.

# Preprocessing #

The figure on the [NTNU](NTNU.md) contribution page shows the structure of the preprocessing. However only the first two parts under preprocessing has been modified at [NTNU](NTNU.md).

These modifications was made in order to add support for the Delsys Trigno
Wireless System, which is the primary sensor system at [NTNU](NTNU.md). There is now available support for Delsys Trigno Wireless System in [Recordings](SigRecordings.md) and [Recording Session](SigRecordings.md).

Delsys Trigno Wireless System is a multimodul unit which is a sensor unit that consists of more then one type of sensors. Under is a description of the sensor system and the new sensor data.
  * [Delsys Trigno Wireless System](DTWS.md)
  * [Accelerometer support](Acceleromter.md)