# Introduction #


The Fitts' Law Test (FLT) is a tool that can be used to evaluate the real-time performance of EMG pattern recognition schemes by moving a cursor on a screen in up to three degrees-of-freedom (DOFs). Originally proposed by Fitts in 1954, the Fitts’ Law test has become an international standard (ISO9341-9) for the validation of practically any type of human-computer interface (HCI) including mice, joysticks, touchpads, or even eye-trackers (Zhang & MacKenzie, 2007). 

Although the Fitts’ Law test is most commonly applied to two-dimensional tasks, our test is modeled after the pseudo 3-D Fitts’ Law task proposed by Scheme and Englehart (2013), where a third DOF is incorporated by allowing control of the size of the cursor.

> [E.  J.  Scheme  and  K.  B.  Englehart,  “Validation  of  a selective  ensemble-based  classiﬁcation  scheme  for myoelectric  control  using  a  three-dimensional  Fitts’  law test,”  IEEE  Trans. Neural Syst.  Rehabil. Eng., vol. 21,  pp. 616-623, 2013](https://www.researchgate.net/publication/233799036_Validation_of_a_Selective_Ensemble-Based_Classification_Scheme_for_Myoelectric_Control_Using_a_Three-Dimensional_Fitts'_Law_Test)

One of the most important metrics that can be derived from the Fitts ‘ Law test is called the throughput (TP), which is defined as

 **TP = ID/MT          (bits/second)**

where _MT_ is the time (in seconds) taken to acquire the target and _ID_ is the index of difficulty (in bits), which is defined as

 **ID = log2( (D/W) + 1)        (bits)**

where _D_ is the distance to the target and _W_ is the target width.


# Process #

In our Fitts’ Law test, two circles appear on the screen. The first, a medium-sized circular cursor at the center of the screen, and the second, a “target” that is located at a pre-defined “distance” from the first circle and has a pre-defined “width”. The subject is then instructed to move the cursor (up, down, right, left, expand circle, shrink circle) in order to match the location and size of the target circle. The cursor must remain within the target width for a certain period of time, which is defined by the Dwell Time parameter visible on the start screen.

Each "trial" is comprised of a series of target acquisition tasks, which are determined by the chosen target parameters (Target Distances, Target Widths, Distance Type, and Target DOF) and are presented in a random order.

Data and metrics from each test are processed and organized via FittsTestMetrics.m and finally stored in the structure fittsTest. Once the test is complete, important metrics such as completion rate, path efficiency, overshoot, stopping distance, and throughput and a graph plotting completion time vs index of difficulty can be displayed by pressing “View Results” in the Fitts’ Law Test GUI. 

Additionally, if data from a previous Fitts’ Law Test wants to be viewed, it can be loaded by pressing “Load Previous Fitts Test” in the menu bar. Once loaded, clicking “View Results” will display the results of the loaded Fitts’ Law test.

# Configuration #

Before starting the test there are a number of variables that can be altered. These can be changed to alter the length, repetition and difficulty of the test.
The parameters are:
* **Trials** - The amount of trials to do during the test.
* **Time out (s)** - The total time to allowed to reach the target
* **Repetitions** - The amount of repetitions to do for each target acquisition task. (Note: when 'Distance Type: Per DOF' is selected, this field will become uneditable and instead show the number of distinct target positions available as determined by the DOFs given to the cursor (Extend Hand, Flex Hand, Open Hand, etc.), and the DOFs given to the target ('Target DOF').)
* **Dwell time (s)**- The required time the subject must stay within the width of the target to complete a task.
* **Target Distances (deg)** - Array of target distances. Must match length of Target Widths. Separate with commas.
* **Target Widths (deg)** - Array of target distances. Must match length of Target Widths. Separate with commas.
* **Distance Type** - "Per DOF" or "Euclidean" distance
 * _Euclidean_ - The locations of the targets in the trial will be chosen randomly from the set of coordinates whose Euclidean distance from the origin equals the input distance value. That is, in the case of a cursor with 3 DOFs
 Distance = sqrt((x_distance)^2+(y_distance)^2+(r_distance)^2)
E.g. If distance = 60, a possible set of coordinates in 3 dimensions would be (49.95,-23,24). Or in 2 dimensions a possible set of coordinates would be (-36,0,48). The number of tasks using each index of difficulty will be equal to the number of repetitions multiplied by the number of trials.
 * _Per DOF_ - Rather than Euclidean distance, input distance values will apply to each DOF allowed. E.g. If distance = 60, a possible set of coordinates when Target DOF = 3 would be (60,60,-60). If Target DOF = 1, a possible set of coordinates would be (0,60,0). Furthermore, in this setting care is taken to treat target direction as a significant parameter, with the total number of target directions being represented by the "Repetitions" input. Here, the number of tasks using each distinct combination of index of difficulty and target direction will equal the number of trials.
  
In addition, the direction controls for the cursor (Expand Circle, Shrink, Circle, Right, Left, Up, and Down) can be customized to accommodate Fitts’ Law tests of 1, 2, or 3 degrees of freedom using any movement input (Open Hand, Close Hand, Flex Hand, Pronation, etc.) that exists within the PatRec structure. 

**Note:** in order to work, each direction that is assigned an input movement must have its reciprocal direction selected too (Expand and Shrink, Right and Left, Up and Down), or else neither direction will be included in the test.

The speed (deg/s) of cursor movement can be set in the Real-Time PatRec GUI prior to opening the Fitts' Law Test GUI.

# Recommendations #
The following test parameters are recommended for a test with six movements:
* Trials: at least 1
* Time out: 15 seconds
* Repetitions: 6
* Dwell Time: 1 second
* Target Distances: 30,30,60,60
* Target Widths: 13,18,13,18
* Distance Type: Per DOF
* Target DOF: 3
* Speed: 2 deg/s

**Cursor Controls**
* Right: Extend Hand (Flex Hand if using left arm)  
* Left: Flex Hand (Extend Hand if using right arm)  
* Up: Supination  
* Down: Pronation  
* Expand Circle: Open Hand  
* Shrink Circle: Close Hand  


