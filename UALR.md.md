
Dr. Afzal contributed with a virtual leg implemented in BioPatRec TVÅ during his doctoral work at University of Arkansas at Little Rock (UALR). The source code is available "as received" from Dr. Afzal at [BioPatRec_TVA_UALR_VirtualLeg0](https://github.com/biopatrec/biopatrec/tree/contributions/BioPatRec_TVA_UALR_VirtualLeg0), together with the documentation below. For information on this development you can mail taimoorafzal@gmail.com

SVN checkout address: https://github.com/biopatrec/biopatrec/branches/contributions/BioPatRec_TVA_UALR_VirtualLeg0

## Introduction

A virtual leg model for simulating non-weight bearing movements was built in Blender 2.7. The model was exported to OGRE3D environment where it was simulated. The model is capable of performing the following movements.

1.       Knee Extension
2.       Knee Flexion
3.       Femur Rot In
4.       Femur Rot out
5.       Tibia Rot in
6.       Tibia Rot out
7.       Ankle Plantarflexion
8.       Ankle Dorsiflexion

The simulated model is interfaced with Matlab and can be controlled via existing TCP/IP protocol in BioPatRec.

## Bone Hierarchy
The model is composed of four bones. Each bone is capable of contributing one degree of freedom. The femur bone is responsible for femur rotation in and out. The knee joint bone is responsible for knee extension and flexion. The tibia bone is capable of tibia rotation in and out while the ankle joint bone is responsible for ankle dorsiflexion and plantarflexion.

## Range of motion (ROM)
The ROM of each of the joints is comparable to the human’s biological joint ROM. However since the purpose of the simulated leg is to measure the effectiveness of control algorithms based on EMG signal processing which can be measured if each movement takes the same degrees to complete. So if the leg is at rest and the knee is extended, the algorithm has to classify 40 correct movements to get the knee to the extended position even though the knee angle in extended form is 70 degrees, thus knee extends 70/40 degrees on every decision. Similarly ankle dorsiflexion is only 15 degrees, but since the classification algorithm is required to identify 40 correct decisions to get the ankle dorsiflexed, the ankle dorsiflexes only 15/40 degrees for every correct decision.

## Reference
Afzal T. Novel methodologies for locomotion mode identification in lower limb prosthesis. Doctoral Thesis at the Department of Systems Engineering, University of Arkansas at Little Rock. May 2015. 