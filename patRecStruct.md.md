[recSession](recSession.md) -> [sigTreated](sigTreated.md) -> [sigFeatures](sigFeatures.md) -> [patRec](patRec.md)

# patRec #
  * nM (number of movements)
  * mov (movements)
  * sF (sampling frequency)
  * tW (time window)
  * nCh (vector with the number of channels)
  * dev (device used for recordings)
  * fFilter (frequency filter)
  * sFilter (spatial filter)
  * selFeatures (selected features ID, cell)
  * topology
  * featureReduction (structure for the feature reduction algorithm if any)
  * movOutIdx (index for the movements related to the output
  * normSets (normalization information, struct)
  * floorNoise (average feature value during the resting period)
  * patRecTrained
  * trTime (training time)
  * tTime (testing time)
  * acc (accuracy per movement plus the over all accuracy in the last row)
  * confMat (confusion matrix)
  * date

According to the algorithm used (see [PatRec](\PatRec.md)), additional variables in patRecTrained contain the required information for applying the trained classifier. Same applies for the information saved in featureReduction.