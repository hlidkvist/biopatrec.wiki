# Who's in? #

  * Max Ortiz Catalan, PhD.
    * Natural Control of Artificial Limbs Through an Osseointegrated Implant
    * **Chalmers University of Technology / Centre for Advanced Reconstruction of Extremities / Integrum AB, Sweden**
    * BioPatRec developer

  * Morten Bak Kristoffersen, MSc.
    * Medialogy
    * **Chalmers University of Technology / Integrum AB, Sweden**
    * Originally at **Aalborg University, Denmark**
    * BioPatRec interest: Augmented Reality

  * Enzo Mastinu, PhD.c
    * Prosthetic control and digital electronics
    * **Chalmers University of Technology / Integrum AB, Sweden**
    * BioPatRec developer

## Pre-release versions of BioPatRec have been shared to: ##

  * Tim Gale, BE, MEngSc, PhD.
    * Senior Lecturer: Mechatronics and Medical Engineering
    * **University of Tasmania, Australia**
    * BioPatRec interest: Signal processing and PatRec

  * Ganesh, Naik, PhD.
    * Chancellor's Post Doctoral Research Fellow, A/DRsch Ctre for Health Technologies
    * **University of Technology Sydney, Australia**
    * BioPatRec interest: Myoelectric signal processing and PatRec

  * Tsvi Achler, MD, PhD.
    * Cognitive Computing
    * **IBM Research, Almaden, USA**
    * BioPatRec interest: PatRec

  * Jie Liu, PhD.
    * The Sensory Motor Performance Program
    * **Northwestern University, Rehabilitation Institute of Chicago, USA**
    * BioPatRec interest: BioPatRec applied to exoskeletons

  * Christian Antfolk, PhD.
    * Dept. of Measurement Technology and Industrial Electrical Engineering
    * **Lund University, Sweden**
    * BioPatRec interest: PatRec

  * Alexander Boschmann, PhD.c
    * Research Associate, Computer Engineering Group
    * **University of Paderborn, Germany**
    * BioPatRec interest: PatRec and SVM.

  * Cosima Prahm, PhD.c
    * Department of Surgery, CD Laboratory for Bionic Reconstruction
    * **Medical University of Vienna, Austria**
    * BioPatRec interest: Real-time control and rehabilitation

  * Daphne Boere, BME, PhD.c
    * Myoelectric transradial prosthesis control
    * **Roessingh Research and Development / University of Twente, the Netherlands**
    * BioPatRec interest: Myoelectric signal processing and PatRec

  * Vincent Hall, PhD.c
    * Molecular Organisation and Assembly in Cells
    * **University of Warwick, England**
    * BioPatRec interest: PatRec

  * Ali H. Al-Timemy, PhD.c, and Peidong Liang, PhD.c
    * Centre for Robotics and Neural Systems
    * **Plymouth University, England**
    * BioPatRec interest: Myoelectric prosthetic control

  * Alejandra Aranceta-Garza, PhD.c
    * Neurohpysiology Bioengineer
    * **Strathclyde University, Scotland**
    * BioPatRec interest: Myoelectric signal processing and PatRec

  * Ghulam Rasool, PhD.c
    * Systems Engineering Department
    * **University of Arkansas at Little Rock, USA**
    * BioPatRec interest: Signal processing and myoelectric control

  * Nichlas Sander, MSc.
    * Biomedical Engineering and Computer Science
    * **Chalmers University of Technology / Integrum AB, Sweden**
    * [VRE](VRE.md) developer

  * Veronika Handrick, MSc.c
    * Biomedical Eng.
    * **University of Applied Sciences Jena, Germany**
    * BioPatRec interest: General

  * Alf B. Bertnum, MSc.c / Kristian Berrum, MSc.c
    * Engineering Cybernetics
    * **Norwegian University of Science and Technology, Norway**
    * BioPatRec interest: Initial contribution to [NTNU](NTNU.md) section.

  * Joel Falk-Dahlin, MSc.c
    * Complex Adaptive System, Applied Physics.
    * **Chalmers University of Technology, Sweden**
    * BioPatRec interest: Post-processing and control

  * Luca Caltagirone, MSc.c
    * Complex Adaptive System, Applied Physics.
    * **Chalmers University of Technology, Sweden**
    * BioPatRec interest: Signal processing and PatRec

  * Joakim Arver, MSc.c
    * Biomedical Eng.
    * **Chalmers University of Technology, Sweden**
    * BioPatRec interest: Initial contributions on the [VRE](VRE.md)

  * Ali Fouad, MSc.c / Siva Kumar, MSc.c
    * Complex Adaptive Systems / Biomedical Eng.
    * **Chalmers University of Technology, Sweden**
    * BioPatRec interest: [PCA](PCA.md), [ICA](ICA.md), [KNN](KNN.md) and [SOM](SOM.md)

  * Prathamesh Dhanpalwar, MSc.c
    * Biomedical Eng.
    * **Chalmers University of Technology, Sweden**
    * BioPatRec interest: Myoelectric signals acquisition

  * Per Förstberg, MSc.c / Gustav Josefsson, MSc.c
    * Biomedical Eng.
    * **Chalmers University of Technology, Sweden**
    * BioPatRec interest: Analog front ends and myoelectric signal acquisition

  * GauravJit Singh, BS.c
    * Biomedical Eng.
    * **Drexel University, USA**
    * BioPatRec interest: Prosthetic devices and control


Note: To avoid hurting anyone's feelings, the title used corresponds to the time in which they initially received BioPatRec, unless otherwise requested.