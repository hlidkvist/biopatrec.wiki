# Introduction #

The roadmaps provide a quick and general overview of the most relevant function within BioPatRec.

  * [RoadMap\_ETT](RoadMap_ETT.md)
  * [RoadMap\_TVA](RoadMap_TVA.md)
  * [RoadMap\_TRE](RoadMap_TRE.md)
  * [RoadMap\_FRA](RoadMap_FRA.md)
