NOTE: This is an ongoing development and therefore the documentation is not yet finalised.

# Intent interpretation #

## Simultaneous proportional control ##
As seen on the front page of the [NTNU](NTNU.md) contribution, there is figure showing the details for this part. It is divided into two parts, containing three operations for simultaneous proportional control:
  * [Linear mapping](NTNU_Linear_mapping.md)
  * [Nonlinear flutter rejection filter](NTNU_Nonlinear_flutter_rejection_filter.md)
  * [Gain and threshold adjustments](NTNU_Gain_and_threshold_adjustments.md)

These operations together make the intent interpretation for the simultaneous proportional control.