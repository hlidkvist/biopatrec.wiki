# Introduction #
The idea behind the protocol is to have an easy system that can be easily implemented onto any existing system, not only the [VRE](VRE.md), like for this case.

To limit the amount of data being sent each transfer, each DoF is set up within the [VRE](VRE.md) and is handled separately. So to perform a movement only one transfer is needed.

## Protocol ##
The protocol consists of four bytes being transferred each time. The different setups are shown, and explained, in the table below:

|`[Byte]` - Operation|Value #1|Value #2|Value #3|Value #4|
|:-------------------|:-------|:-------|:-------|:-------|
|`[1]` - Movement of the limb|Indicating which DoF|Indicating which direction to move|Indicating distance to move in given direction|Indicating the fraction|
|`[2]` - Movement of TAC|Indicating which DoF|Indicating which direction to move|Indicating distance to move in given direction|Indicating the fraction|
|`[c]` - Configuration|See below|for more|information.|Not specified.|
|`[r]` - Reset position|If set to `[t]`, resets position of TAC. Otherwise the normal hand.|Not specified.|Not specified.|Not specified.|

An example of the protocol is given:
```
'12150' - The normal hands(or leg) 2nd DoF is extended 5 steps (fraction 0 -> 1).
'23032' - The TAC hands(or leg) 3rd DoF is flexed 3/2 steps.
'rt00' - The position of the TAC hand is reset.
```

## Configurations ##

If anything needs to be configured in the Virtual Reality Environment, a configuration parameter can be sent in order to change vital parameters without editing the code.

Below are all the codes with a short explanation of how to use it.

In order to edit any information you must first send c as the Operation Byte. (see above) Once that is set the four possible values can be used, as shown in the table below, to edit the values of the VRE.

|Value #1|Value #2|Value #3|Value #4|Description|
|:-------|:-------|:-------|:-------|:----------|
|1       |value|(not used)|(not used)|if value is 1 do upper limb, if value is 2 do lower limb |
|2       |(not used)|(not used)|(not used)|Switch TAC test on/off.|
|3       |value|(not used)|(not used)|Set the allowed distance that the user can be from the TAC test in order for it to still be classified as completed.  allowance = (value/100)|
|4       |Camera  |(not used)|(not used)|Set which camera to look through.|
|5       |(not used)|(not used)|(not used)|Switch between right and left limb.|
|6       |(not used)|(not used)|(not used)|Switch between above elbow/knee and beneath elbow/knee.|
|7       |(not used)|(not used)|(not used)|Switch sending ACK messages on and off.|
|8       |value     |(not used)|(not used)|Switch sTAC on (value >0) or off.|



##movements.def##
The movements are defined in the movements.def file. The first number in each row is byte that specify the movement labelled as the following string. The two numbers that follows are value #2 and value #3 of the VRE protocol. For more information about the movement list see [movements.def](Protocol.md) 
```
0,Rest,0,0,0 0
1,Open Hand,9,1,1 1
2,Close Hand,9,0,1 0
3,Flex Hand,7,0,0 1
4,Ext Hand,7,1,0 0
5,Pronation,8,0,2 0
6,Supination,8,1,2 1
24,Side Grip,13,1,0 0
23,Fine Grip,12,1,0 0
22,Agree,11,1,0 0
17,Point,10,1,0 0
7,Thumb Ext,5,1,0 0
8,Thumb Flex,5,0,0 0
19,Thumb Yaw Ext,6,1,0 0
18,Thumb Yaw Flex,6,0,0 0
20,Flex Elbow,15,1,3 0
21,Ext Elbow,15,0,3 1
9,Index Ext,4,1,0 0
10,Index Flex,4,0,0 0
11,Middle Ext,3,1,0 0
12,Middle Flex,3,0,0 0
13,Ring Ext,2,1,0 0
14,Ring Flex,2,0,0 0
15,Little Ext,1,1,0 0
16,Little Flex,1,0,0 0
31, Flex Knee,8,1,0 0
32, Extend Knee,8,0,0 0
28, Ankle Dorsilflexion,6,1,0 0
27, Ankle Plantarflexion,6,0,0 0
33, Tibial Rotation in,9,0,0 0 
34, Tibial Rotation out,9,1,0 0
35, Femoral Rotation in,10,1,0 0
36, Femoral Rotation out,10,0,0 0
29, Ankle Inversion,7,0,0 0
30, Ankle Eversion,7,1,0 0
37, Curl Toes,11,0,0 0
38, Stretch Toes,11,1,0 0
9,Index Ext,4,0,0 0
10,Index Flex,4,1,0 0
11,Middle Ext,3,0,0 0
12,Middle Flex,3,1,0 0
13,Ring Ext,2,0,0 0
14,Ring Flex,2,1,0 0
15,Little Ext,1,0,0 0
16,Little Flex,1,1,0 0
25, Big Flex,5,0,0 0
26, Big Ext,5,1,0 0
```
## DoF Map ##
Degrees of freedom for the hand and of the leg defined with the same number. This is possible since two different VREs are used.
Information regarding which DoF corresponds to which movements can be found in the table below.

|DoF|Hand movement|Leg/Foot movement
|:--|:-------|:-------|
|1  |Pinky/Little - Pitch|Little Ext/Flex|
|2  |Ring Finger - Pitch|Ring Toe Extend/Flex|
|3  |Middle Finger- Pitch|Middle Toe Ext/Flex|
|4  |Index Finger - Pitch|Index Toe Ext/Flex|
|5  |Thumb - Pitch| Big Toe Ext/Flex|
|6  |Thumb - Yaw|Ankle Plantarflexion/Dorsiflexion|
|7  |Palm - Pitch|Ankle Inversion/Eversion|
|8  |Palm - Roll|Extend/Flex Knee|
|9  |Open/Close Hand|Tibial Rotation In/Out|
|10 |Point|Femoral Rotation In/Out|
|11 |Agree|Toes Curl/Stretch|