# Statistic for PatRec #

BioPatRec allows you to generate statistics on the accuracy of the offline prediction. The statistics are computed over a selected number of repetitions of the offline training and this can be performed on:

  * Current data. This mean the loaded [sigFeatures](sigFeatures.md)
  * Group. It will load each `*.mat` file containing [sigFeatures](sigFeatures.md) from the selected folder. Note that the files must be named in the following way: 1t.mat, 2t.mat, and so on (e.g. the files returned by the option 'Treat Folder' in the Signal Treatment GUI).

These routines will run OfflinePatRec with the selected settings in the GUI.