# From TVÅ to TRE #

## Additional variables for recSession: RampParameters ##
In order to implement the ramp recording session was necessary to add several variables to the recSession structure. For further detail check out the new data structure at [recSession](recSession.md).

## Functions for other custom acquisition devices ##
In BioPatRec\_TRE are available a new set of functions useful to implement your own custom acquisition device and use it during your PatRec sessions. Check out into the [COMM](COMM.md) section.

## New Recording GUI ##
A new GUI is now available for your recording session. To do that, it has been necessary to make several little changes in functions such as DataShow, RecordingSession etc..

## Prosthetic Devices Control Framework ##
The compatibility of BioPatRec with any commercially available robotic hardware has been improved by developing the so called _control framework_. With respect to the previous version of BioPatRec (which allowed to send driving commands only), a bi-directional communication protocol has been implemented in the TRE release which allows both to drive motors and receive information from eventual sensors available in the controlled device.

Please check the [Control](Control.md) section for more details.

# From ETT to TVÅ #

## Additional variable for OfflinePatRec: algConf ##
Since [SOM](SOM.md) and [SSOM](SSOM.md) needed special configurations, the struct variable _algConf_ is now available for transiting configuration parameters. The calling of the following routines was modified to include the _algConf_ variable inmediatly after _tType_.

  * `OfflinePatRec`
  * `OfflinePatRecTraining`

In order to use the struct _algConf_, just save it in the [GUI\_PatRec](GUI_PatRec.md) handles. The it will be sent to `OfflinePatRec` by `pb_RunOfflineTraining_Callback`. This variable is saved inside patRec.patRecTrained.

## Control algorithms ##

The way the control algorithms are added changed. See: [Control](Control.md) for details