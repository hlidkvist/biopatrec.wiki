# Introduction #
[Prensilia SRL](http://www.prensilia.com/) is a company spun out from [Scuola Superiore Sant'Anna](http://www.sssup.it/), Pisa, in 2009 by researchers of the Advanced Robotics and Technology (ARTS) Laboratory (now [Artificial Hand Area](http://sssa.bioroboticsinstitute.it/research/artificial_hands)), with long experience in research and development of innovative robotic and biomedical devices. Scuola Superiore Sant'Anna is a worldwide recognized University, being one of the pioneers and key actors for research on thought-controlled hands for the past 20 years. Such technology and know-how is now available to research institutes, commercialized and customized by Prensilia.

In particular, Prensilia commercializes [IH2 Azzurra](http://www.prensilia.com/index.php?q=en/node/40), one of the most advanced self-contained robotic hands available on the market, which has been widely used in the past in research projects involving invasive and non-invasive control based on pattern recognition algorithms. In this page, a software interface between BioPatRec and IH2 Azzurra is made freely available.

# IH2 Azzurra kit for BioPatRec #
The kit provides a software interface between BioPatRec and IH2 Azzurra. The interface is based on the control framework introduced with BioPatRec TRE and, as required, comprises the following three .def files and three Matlab functions:

    motors.def
    movements.def
    sensors.def
    ReadSensors.m
    SendMotorCommands.m
    TestConnection.m

Information about the particular implementation of the software interface is briefly reported below. If you would like to know more about the purpose of each file and how it is used in BioPatRec please refer to the wiki [Control](Control.md) page. If you would like to know more about the IH2 Azzurra communication protocol, please refer to its manual available on the [Prensilia website support page](http://www.prensilia.com/index.php?q=en/node/82).


## motors.def ##
In the current implementation, all IH2 Azzurra motors are initialized as DC motors by default (the second column of the file is set to `0`), thus a speed (PWM) control will be used. However, the BioPatRec SendMotorCommand function supports position control as well. Thus if you prefer the latter, you just have to set to `1` the second column of the file.

## movements.def ##
The most common grip patterns physically achievable by IH2 Azzurra were defined. Other movements were not associated to any motor so that, even if issued by BioPatRec, no movement will occur. Feel free to update the movements.def file if  additional movements are required.

## sensors.def ##
The sensors.def file defines the so called External Sensors available in IH2 Azzurra. If you want to record data from other sensors available in IH2 Azzurra (e.g. encorder, current sensors), you should modify both this file and the ReadSensors.m function.

## TestConnection.m ##
The connection with the device is tested by requesting the position of two fingers (the thumb and index finger) and checking that the hardware sends back two bytes (one for each request), as expected. An error code is returned if a different number of bytes is returned. No check is made on the content of the returned message.

## ReadSensors.m ##
The function takes as inputs the IDs (as defined in the first column of the sensors.def file) and the number of sensors to be acquired from the hardware and returns the readings from those sensors. Basically, the function implements the (HLHC) GetExternalSensor command.

## SendMotorCommands.m ##
This function supports both control types available in BioPatRec at the time of writing (August 2015): position and speed control. Both control types are implemented by using IH2 Azzurra HLHC commands. In particular, the position control is implemented by issuing the SetFingerPosition command, while the speed control is implemented through the MoveMotor command. Other control modes available with IH2 Azzurra will likely be integrated in the future as soon as they will be supported in BioPatRec, but feel free to modify this function if you want to add them by your own.
This is indeed pretty easy: just add another case to the switch structure and put your code there. The code should translate the inputs of the SendMotorCommands.m function into commands available within the IH2 Azzurra communication protocol. Take as an example the two control modes already implemented. Then do not forget to update the motors.def file in order to match with the new case statement.

# Download #
The IH2 Azzurra kit for BioPatRec is available for download from [here](http://www.prensilia.com/files/support/software/IH2Azzurra-BioPatRec-v1.zip).