**NOTE: We are just moving from Google Code!!! so there might be few problems in the wiki, mostly related to images and descriptions.**

[BioPatRec](https://github.com/biopatrec/biopatrec/wiki/BioPatRec.md) is a research platform (on MATLAB) that allows you to "easily" implement algorithms to be tested in a real-life and challenging problem: **The control of artificial limbs via decoding (or pattern recognition) of bioelectric signals**. Algorithms in the following areas apply:

* Signal processing
* Features extraction and selection
* Machine learning (pattern recognition) or model-based decoders
* Real-time control

[BioPatRec](https://github.com/biopatrec/biopatrec/wiki/BioPatRec.md) is a modular platform that allows you to focus in your particular interest and simply use the other modules as support for the evaluation of your algorithm. [BioPatRec](https://github.com/biopatrec/biopatrec/wiki/BioPatRec.md) includes a [Virtual Reality Environments](https://github.com/biopatrec/biopatrec/wiki/VRE.md) for a qualitative and quantitative performance evaluation.

You can browse the [wiki](https://github.com/biopatrec/biopatrec/wiki) to find out more, and we recommend you to use the navigation menu on the right side, starting in page [BioPatRec](https://github.com/biopatrec/biopatrec/wiki/BioPatRec.md). Additionally, you can visit the [Highlights](https://github.com/biopatrec/biopatrec/wiki/BioPatRec_Highlights.md) page to see more details on BioPatRec's features and some demos (videos). 

Please read the [source code](https://github.com/biopatrec/biopatrec/wiki/SourceCode.md) page for instructions on how to access the code and data repository.

Reference article for this platform: [Ortiz-Catalan, M., Brånemark, R., and Håkansson, B., “BioPatRec: A modular research platform for the control of artiﬁcial limbs based on pattern recognition algorithms”, Source Code for Biology and Medicine, 2013, 8:11](http://www.scfbm.org/content/8/1/11/)

Would you like to contribute and sum efforts to improve amputees’ quality of life? Join this project!!! or simply send your comments to: maxo@chalmers.se